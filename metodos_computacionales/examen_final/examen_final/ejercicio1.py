# Ejercicio1
# A partir de los arrays x y fx calcule la segunda derivada de fx con respecto a x. 
# Esto lo debe hacer sin usar ciclos 'for' ni 'while'.
# Guarde esta segunda derivada en funcion de x en una grafica llamada 'segunda.png'

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import numpy as np

x = np.linspace(0,2.,10)
fx = np.array([0., 0.0494, 0.1975, 0.4444, 0.7901,1.2346 , 1.7778, 2.4198, 3.1605, 4.])


def derivada(j):
    i = np.int(j)
    delta_x = (x[i+1]-x[i-1])/2.0
    sol = (fx[i+1] - (2*fx[i]) + fx[i-1])/(delta_x*delta_x)
    return sol

sec_der = np.array([derivada(1),derivada(2),derivada(3),derivada(4),derivada(5),derivada(6), derivada(7), derivada(8)])

x_new = [x[1],x[2],x[3],x[4],x[5],x[6],x[7],x[8]]


plt.plot(x_new, sec_der)
plt.savefig('segunda.png')
plt.close()