# Ejercicio5
# Resuelva el siguiente sistema acoplado de ecuaciones diferenciales 
# dx/dt = sigma * (y - x)
# dy/dt = (rho * x) - y -(x*z)
# dz/dt = -beta * z + x * y
# con sigma = 10, beta=2.67, rho=28.0,
# condiciones iniciales t=0, x=0.0, y=0.0, z=0.0, hasta t=5.0.
# Prepare dos graficas con la solucion: de x vs y (xy.png), x vs. z (xz.png) 

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np

sigma = 10.0 
beta = 2.67
rho = 28.0

t = 0.0
x = 0.0
y = 0.0
z = 0.0

h = 0.01
t_max = 5.0

# Se va a evolucionar usando un método Runge-Kutta de orden 4 

def ev_x(x,y):
    sol = sigma*(y-x)
    return sol

def ev_y(x,y,z):
    sol = (rho*x) - y - (x*z)
    return sol

def ev_z(x,y,z):
    sol = -(beta*z) + (x*y)
    return sol

t_ev = [t]
x_ev = [x]
y_ev = [y]
z_ev = [z]

while(t<t_max):
    
    k1_x = ev_x(x,y)
    k1_y = ev_y(x,y,z)
    k1_z = ev_z(x,y,z)
    
    k2_x = ev_x(x+k1_x,y+k1_y)
    k2_y = ev_y(x+k1_x,y+k1_y,z+k1_z)
    k2_z = ev_z(x+k1_x,y+k1_y,z+k1_z)
    
    k3_x = ev_x(x+k2_x,y+k2_y)
    k3_y = ev_y(x+k2_x,y+k2_y,z+k2_z)
    k3_z = ev_z(x+k2_x,y+k2_y,z+k2_z)
                
    k4_x = ev_x(x+k3_x,y+k3_y)
    k4_y = ev_y(x+k3_x,y+k3_y,z+k3_z)
    k4_z = ev_z(x+k3_x,y+k3_y,z+k3_z)
    
    x = x + ((h/6)*(k1_x + (2*k2_x) + (2*k3_x) + k4_x))
    y = y + ((h/6)*(k1_y + (2*k2_y) + (2*k3_y) + k4_y))
    z = z + ((h/6)*(k1_z + (2*k2_z) + (2*k3_z) + k4_z))    
    
    t = t+h
    
    x_ev.append(x)
    y_ev.append(y)
    z_ev.append(z)
    t_ev.append(t)
    
plt.plot(x_ev,y_ev)
plt.xlim(xmin=-2)
plt.savefig('xy.png')
plt.close()

plt.plot(x_ev,z_ev)
plt.xlim(xmin=-2)
plt.savefig('xz.png')
plt.close()






