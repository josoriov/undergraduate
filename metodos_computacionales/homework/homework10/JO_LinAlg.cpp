#include <iostream>

using namespace std;

using std::cout;
using std::endl;

double ** get_matrix(int M, int N);
double ** matrix_product(int M1, int N1, int M2, int N2 ,double ** mat1, double ** mat2);


int main(){
	// M refiere a filas N refiere a columnas, el número refiere a la matriz a la que pertenecen
	int M1, M2, N1, N2, i, j;
	double **mat1;
	double **mat2;

	cout << "Ingrese filas y columas de la matriz A presione enter después de ingresar cada valor ";
	cin >> M1 >> N1;
	cout << "Ingrese filas y columas de la matriz B presione enter después de ingresar cada valor ";
	cin >> M2 >> N2;

	while (N1!=M2)
    {
        cout << "Las columnas de la primera matriz A no son iguales a las filas de la matriz B. No se puede multiplicar.";

        cout << "Ingrese filas y columas de la matriz A presione enter después de ingresar cada valor ";
        cin >> M1 >> N1;

        cout << "Ingrese filas y columas de la matriz B presione enter después de ingresar cada valor ";
        cin >> M2 >> N2;
    }
    cout << endl << "Ingrese los elementos de la matriz A:" << endl;
    mat1 = get_matrix(M1,N1);
    cout << endl << "Ingrese los elementos de la matriz B:" << endl;
    mat2 = get_matrix(M2,N2);
    cout << endl;

    double ** sol = new double*[M1];
    // Inicializamos la matriz.
	for(i=0; i<4; i++){
		sol[i] = new double[N2];
	}
    sol = matrix_product(M1, N1, M2, N2, mat1, mat2);

    cout << "La matriz resultado es:" << endl;

    for(i=0;i<M1;i++){
    	for(j=0;j<N2;j++){
    		cout << sol[i][j] << " ";
    	}
    	cout << sol[i][j] << endl;
    }
	

	return 0;
}

double ** matrix_product(int M1, int N1, int M2, int N2 ,double ** mat1, double ** mat2){
	double ** prod = new double*[M1];
	int i;
	int j;
	int k;
	// Inicializamos la matriz resultado
	for(i=0;i<M1;i++){
		prod[i] = new double[N2];
	} 

	// Llenamos la matriz de resultados con ceros
	for(i=0;i<M1;i++){
		for(j=0;j<N2;j++){
			prod[i][j] = 0;
		}
	}

	// Llenamos la matriz con el resultado
	for(i=0;i<M1;i++){
		for(j=0;j<N2;j++){
			for(k=0;k<N1;k++){
				prod[i][j] += mat1[i][k] * mat2[k][j]; 
			}
		}
	}

	return prod;

}

double ** get_matrix(int M, int N){

	double **mat = new double*[M];

	int i;
	int j;
	// Inicializamos la matriz.
	for(i=0; i<M; i++){
		mat[i] = new double[N];
	}

	// Llenamos la matriz con ceros
	for(i=0;i<M;i++){
		for(j=0;j<N;j++){
			mat[i][j] = 0;
		}
	}

	// Llenamos la matriz con valores de consola
	for(i=0;i<M;i++){
		for(j=0;j<N;j++){
			cout << "Ingrese el elemento " << i+1 << j+1 << " : ";
			cin >> mat[i][j];
		}		
	}

	return mat;
}