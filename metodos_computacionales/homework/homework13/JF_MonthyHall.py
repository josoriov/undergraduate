
# coding: utf-8

# In[111]:

import numpy as np
import matplotlib.pyplot as plt
import random


# In[112]:

# Función que crea un array con los valores de cada una de las 3 puertas
def sort_doors():
    lis = ['goat','goat','car']
    random.shuffle(lis)
    return lis


# In[113]:

# Función que un número aleatorio de entre [0,1,2]
def choose_door():
    return random.randint(0,2)


# In[114]:

# Función que hace el papel de Monthy Hall revelando una de las cabras
def reveal_door(lista, choice):
    l = lista
    for i in range(len(l)):
        if(i != choice and l[i] == 'goat'):
            l[i] = 'GOAT_MONTY'
            break
    return l    


# In[115]:

# Función que finaliza el juego según el usuario decida o no cambiar de puerta
def finish_game(lista,choice,change):
    if(change != True):
        return lista[choice]
    else:
        for i in range(len(lista)):
            if(lista[i] != 'GOAT_MONTY' and i != choice):
                return lista[i]
                break


# In[116]:

true_list = list()
false_list = list()

# Corremos el juego 100 veces y guardamos los resultados para cuando hubo cambio de puerta y cuando no
for i in range(100):
    
    lista = sort_doors()
    choice = choose_door()
    
    lista = reveal_door(lista,choice)
    
    true_list.append(finish_game(lista,choice,True))
    false_list.append(finish_game(lista,choice,False))


# In[117]:
# Contamos cuántas veces salió el premio al cambiarse y no cambiar de puerta
a = true_list.count('car')
b = false_list.count('car')


# In[120]:

print "\n"
print "La probablidad al cambiar de puerta es de "+str(a)+"%"
print "La probablidad al no cambiar de puerta es de "+str(b)+"%"+"\n"


# In[ ]:



