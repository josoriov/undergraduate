#include <iostream>
#include <cstdlib>
#include <ctime>

using std::cout;
using std::endl;

float ev_x(float t, float x, float v);
float ev_v(float t, float x,float M, float G);


int main(){
	float t = 0.0;
	float x = 100.0, x_0 = 100.0;
	float v = 0.0;
	float h = 0.001;
	float M=1000.0,G=10;
	int i=0;
	float k1, k2, k3, k4;
	float k1_x, k2_x, k3_x, k4_x;

	while(x>x_0*0.01 && i< 250000){

		k1 = ev_v(t,x,M,G);
		k2 = ev_v(t+(h/2), x+(h*k1/2), M, G);
		k3 = ev_v(t+(h/2), x+(h*k2/2), M, G);
		k4 = ev_v(t+h, x+(h*k3), M, G);

		k1_x = ev_x(t,x,v);
		k2_x = ev_x(t+(h/2), x+(h*k1/2),v);
		k3_x = ev_x(t+(h/2), x+(h*k2/2),v);
		k4_x = ev_x(t+h, x+(h*k3), v);

		v = v + (h*(k1+(2*k2)+(2*k3)+k4)/6);		

		x = x + (h*(k1_x+(2*k2_x)+(2*k3_x)+k4_x)/6);
		t = t+h;
		i = i+1;

		cout << t << " " << x << " " << v << endl;
		
	}
		

	return 0;
}

float ev_v(float t, float x,float M, float G){
	return -(G*M)/(x*x);
}
float ev_x(float t, float x, float v){
	return v;
}