
# coding: utf-8

# In[7]:

import numpy as np
import matplotlib.pyplot as plt


# In[8]:

A = np.loadtxt('tray.txt')

t = A[:,0]
x = A[:,1]
v = A[:,2]


# In[9]:

plt.plot(x,t)
plt.suptitle(u'Posición')
plt.xlabel('x')
plt.ylabel('t')
plt.savefig('pos.png')
plt.close()


# In[10]:

plt.plot(v,t)
plt.suptitle(u'Velocidad')
plt.xlabel('v')
plt.ylabel('t')
plt.savefig('vel.png')
plt.close()


# In[11]:

plt.plot(v,x)
plt.suptitle(u'Fase')
plt.xlabel('v')
plt.ylabel('x')
plt.savefig('phase.png')
plt.close()


# In[ ]:



