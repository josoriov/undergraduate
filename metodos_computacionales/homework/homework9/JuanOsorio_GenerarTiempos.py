
# coding: utf-8


import numpy as np
import matplotlib.pyplot as plt
import time


# Función que calcula el enésimo número de la serie de Fibonacci

def fibonacci(N):
    if(N<2):
        return N
    else:
        return (fibonacci(N-1)+fibonacci(N-2))


# Función que retorna el tiempo que toma calcular el enésimo número Fibonacci

def get_time(N):
    t0 = time.time()
    fibonacci(N)
    t1 = time.time()-t0
    return t1


# Imprimo la iteración y el tiempo que toma calcular el N número Fibonacci

for i in range(1,36):
    print( str(i) + " " + str(get_time(i)))





