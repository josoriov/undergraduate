#include <iostream>
#include <cstdlib>
#include <ctime>

using std::cout;
using std::endl;

int fibonacci(int x);
float get_time(int x);

int main(){
	int x = 35;

	// Imprimo el tiempo que le toma a c++ calcular el enésimo número de Fibonacci
	for(int i=1;i<=x;i++){
		cout << i << " " << get_time(i) << endl;
	}

	return 0;
}

// Función que calcula el enésimo número de la serie de Fibonacci 
int fibonacci(int x){
	if(x < 2){
		return x;
	}
	else{
		return (fibonacci(x-1)+fibonacci(x-2));
	}
	
}

// Función que retonra el tiempo que toma calcular el enésimo número de la serie de Fibonacci
float get_time(int x){
	clock_t t;
	t = clock();

	fibonacci(x);

	t = clock() - t;
	float time = ((float)t)/CLOCKS_PER_SEC;
	return time;
}