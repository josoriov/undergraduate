
# coding: utf-8


import numpy as np
import matplotlib.pyplot as plt



# Cargo los datos
A = np.loadtxt('times_cpp.csv')
B = np.loadtxt('times_python.csv')


# Tomo las columnas que me interesan de los arrays de datos
iteracion = A[:,0]
c = A[:,1]
python = B[:,1]



# Genero la gráfica con las especificaciones indicadas
plt.plot(iteracion,c, label='c++')
plt.plot(iteracion,python, label='Python')
plt.xlabel(r'$No.$ de la iteracion')
plt.ylabel('Tiempo')
plt.suptitle(r'Fibonacci $t$ vs $No.$')
plt.legend()
plt.savefig('cpp_vs_python.png')

