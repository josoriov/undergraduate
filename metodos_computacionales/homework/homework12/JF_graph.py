
# coding: utf-8

# In[40]:

import numpy as np
import matplotlib.pyplot as plt


# In[41]:

A = np.loadtxt('advection.txt')


# In[ ]:




# In[47]:

x1 = []
x2 = []
x3 = []
x4 = []
x5 = []


# In[48]:

y1 = []
y2 = []
y3 = []
y4 = []
y5 = []


# In[49]:

m,n = np.shape(A)
t_0 = 0.0

for i in range(0,m):
    count = 1
    if(t_0 == A[i,0]):
        if(count == 1):
            x1.append(A[i,1])
            y1.append(A[i,2])
        elif(count == 2):
            x2.append(A[i,1])
            y2.append(A[i,2])
        elif(count == 3):
            x3.append(A[i,1])
            y3.append(A[i,2])
        elif(count == 4):
            x4.append(A[i,1])
            y4.append(A[i,2])
        else:
            x5.append(A[i,1])
            y5.append(A[i,2])
    else:
        count = count + 1
        t_0 = A[i,0]


# In[50]:

plt.plot(x1,y1)
plt.savefig('grafica.png')
plt.xlabel('x')
plt.ylabel(u'Función')


# In[46]:




# In[ ]:



