#include <iostream>
#include <cstdlib>
#include <ctime>

using std::cout;
using std::endl;

int main(){
  double c = 1.0, ini = -2.0;
  double t=0.0, x=-2.0;
  double x_0 = 0.1, t_0 = 0.001;
  int M = (int)(4.0/x_0) + 1; // Puntos en x
  double * datos = new double[M];
  int i, j;

  // Inicializo el array que guarda los datos para cierto tiempo
  for(i=0;i<M;i++){
    double x_i = x + (i*x_0);

    if(x_i>=-0.5 && x_i <= 0.5){
      datos[i] = 0.5;
    }

    else{
      datos[i] = 0.0;
    }
  }

  int n_t = 1;

  while(n_t <= 700){
    
    // Imprimiremos 5 snapshots con determinado tiempo para ver la evolución
    if(n_t%175 == 0 || n_t == 1){

      for(j=0;j<M;j++){
        cout << t << " " << ini + (x_0*j) << " " << datos[j] <<endl;
      }
    }

    t = t_0*n_t;

    for(i=0;i<M;i++){

      if(i==1 || i==(M-1)){
        datos[i] = datos[i];
      }
      else{
        // Evoluciona con backward difference
        double temp = t_0/(c*x_0);
        double temp2 = temp*(datos[i]-datos[i-1]);
        datos[i] = datos[i] - temp2;
      }
    } 
    

    n_t = n_t + 1;

  }

  return 0;
}