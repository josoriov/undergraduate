
# coding: utf-8

# In[3]:

import numpy as np
import matplotlib.pyplot as plt


# In[4]:

A = np.loadtxt('fechas_manchas.dat')

tiempo = A[:,0]
manchas = A[:,1]

plt.plot(tiempo,manchas)
plt.xlabel(u'Año')
plt.ylabel('Manchas')
plt.suptitle('Manchas solares')
plt.savefig('fecha_manchas.pdf')


# In[ ]:



