
# coding: utf-8

# In[1]:

import numpy as np
import matplotlib.pyplot as plt


# In[2]:

A = np.loadtxt('datos_album.txt')


# In[3]:

unicas = A[:,0]
repetidas = A[:,1]
sobres = range(len(repetidas))


# In[5]:

plt.plot(sobres,unicas, label='unicas')
plt.plot(sobres,repetidas, label='repetidas')
plt.legend()
plt.savefig('grafica_album.png')


# In[ ]:



