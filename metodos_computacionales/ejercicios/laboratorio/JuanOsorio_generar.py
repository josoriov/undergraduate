
# coding: utf-8

# In[1]:

import numpy as np
import matplotlib.pyplot as plt


#Genera muestra dados los números y su probabilidad

def sample_1(N):
    A = np.random.choice(a=[-10,-5,3,9],p=[0.1,0.4,0.2,0.3],size=N)
    return A


# Genera muestra que sigue distribución exponencial

def sample_2(N):
    A = np.random.exponential(scale=0.5,size=N)
    return A

# Retorna un array con las M medias de los datos dadas la función de muestreo

def get_mean(sampling_fun,N,M):
    sol = np.zeros(M)
    for i in range(M):        
        A = sampling_fun(N)
        mean = np.mean(A)
        sol[i] = mean
    return sol


# Crea 6 archivos de texto con datos de 10000 medias para ambas distribuciones con N=10,100,1000

M = 10000
N = [10,100,1000]

for i in range(len(N)):
    A = get_mean(sample_1,N[i],M)
    B = get_mean(sample_2,N[i],M)
    
    s1 = 'sample_1_'+str(N[i])+'.txt'
    s2 = 'sample_2_'+str(N[i])+'.txt'
    
    np.savetxt(fname=s1,X=A)
    np.savetxt(fname=s2,X=B)


# In[ ]:



