
# coding: utf-8

# In[5]:

import numpy as np
import matplotlib.pyplot as plt
import scipy.optimize as opt


# Función de distribución normal para una media y una desviación estándar dadas

def normal_dist(x,mean,sigma):
    a = 1/np.sqrt(2*np.pi*(sigma**2))
    b = (x - mean)**2
    c = 2*(sigma**2)
    
    d = a*np.exp(-b/c)
    return d


# Hacemos un fit con el histograma de los datos cargados con el archivo. Sobreponemos el fit con el histograma

def get_fit(filename):
    A = np.loadtxt(filename)
    x,y = np.histogram(A)
    param = opt.curve_fit(normal_dist,x,y)
    
    plt.hist(A)
    plt.plot(x,y)
    plt.show()
    
    print param


# In[10]:

get_fit('sample_1_10.txt')


# In[ ]:



