# coding: utf-8

# In[1]:

import numpy as np
import matplotlib.pyplot as plt


# In[2]:

A = np.loadtxt('dat2.txt')


# In[3]:

x = A[:,0]
y = A[:,1]
z = A[:,2]



# In[4]:

plt.scatter(y,z, color='gray')
plt.savefig('segundo_orden_yz.png')

plt.clf()

plt.scatter(x,y,color='gray')
plt.savefig('segundo_orden_xy.png')