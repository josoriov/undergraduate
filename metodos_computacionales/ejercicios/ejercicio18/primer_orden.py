
# coding: utf-8

# In[1]:

import numpy as np
import matplotlib.pyplot as plt


# In[2]:

A = np.loadtxt('dat.txt')


# In[3]:

x = A[:,0]
y = A[:,1]

x_0 = np.linspace(0,3,50)
y_0 = np.exp(-x_0)


# In[4]:

plt.scatter(x,y, color='gray')
plt.plot(x_0,y_0, color='blue')
plt.savefig('primer_orden.png')


# In[ ]:



