#include <iostream>
#include <cstdlib>
#include <ctime>
#include <math.h> 

using std::cout;
using std::endl;

float gauss(float x);
int max_arg(double * datos, int M);

int main(){
	double D = 1.0;
	double x = 0.0, t=0.0;
	double x_0 = 0.1, t_0 = 0.01;
	int M = (int)(2.0/x_0) + 1; // Puntos en x
	double * datos = new double[M];
	int i, j;

	// Inicializo el array que guarda los datos para cierto tiempo

	for(i=0;i<M;i++){
	double x_i = i*x_0;
	datos[i] = gauss(x_i);
	}

	t = t + t_0;

	int k = max_arg(datos,M);
	// Evolucionamos si no se ha cumplido la condición aún
	while(datos[k]>0.5){

		for(i=0;i<M;i++){

			if(i==0 || i==M-1){
				datos[i] = 0;
			}
			else{
				double temp = D*t_0/(x_0*x_0);
				double temp2 = temp*(datos[i+1] - (2*datos[i]) + datos[i-1]);
				datos[i] = temp2 + datos[i];
			}

		}
		t = t + t_0;
		k = max_arg(datos,M);
	}

	for(j=0;j<M;j++){
		cout << x_0*j << " " << datos[j] << endl;
	}


	return 0;
}

float gauss(float x){
  float sigma = 0.1;
  float a = 2*sigma*sigma;
  float x_0 = 1.0;
  float b = (x-x_0)*(x-x_0)/a;
  return exp(-b);
}

int max_arg(double * datos, int M){
	int i;
	int sol = 0;
	for(i=0;i<M;i++){
		(datos[sol]<datos[i]) ? (sol = i) : (sol = sol);
	}
	return sol;
}