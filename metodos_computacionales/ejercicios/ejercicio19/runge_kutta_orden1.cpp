#include <iostream>
#include <cstdlib>
#include <ctime>

using std::cout;
using std::endl;

float evaluate(float x, float y);


int main(){
	float y = 1.0;
	float x = 0.0;
	float h = 0.1;
	int N = 10/h;
	int i;

	for(i=0; i<N; i++){
		float k1 = evaluate(x,y);
		float k2 = evaluate(x+(h/2), y+(h*k1/2));
		float k3 = evaluate(x+(h/2), y+(h*k2/2));
		float k4 = evaluate(x+h, y+(h*k3));
		y = y + (h*(k1+(2*k2)+(2*k3)+k4)/6);
		x = x+h;
		cout << x << " " << y << endl;
		
	}	

	return 0;
}

float evaluate(float x, float y){
	return -y;
}