#include <iostream>
#include <cstdlib>
#include <ctime>

using std::cout;
using std::endl;

float ev_z(float x, float y);
float ev_y(float x, float y, float z);


int main(){
	float y = 1.0;
	float x = 0.0;
	float z = 0.0;
	float h = 0.1;
	int N = 10/h;
	int i;
	float k1;
	float k2;
	float k3;
	float k4;

	for(i=0; i<N; i++){
		float tempZ = z;
		k1 = ev_z(x,y);
		k2 = ev_z(x+(h/2), y+(h*k1/2));
		k3 = ev_z(x+(h/2), y+(h*k2/2));
		k4 = ev_z(x+h, y+(h*k3));

		z = z + (h*(k1+(2*k2)+(2*k3)+k4)/6);

		k1 = ev_y(x,y,z);
		k2 = ev_y(x+(h/2), y+(h*k1/2),z);
		k3 = ev_y(x+(h/2), y+(h*k2/2),z);
		k4 = ev_y(x+h, y+(h*k3), z);

		y = y + (h*(k1+(2*k2)+(2*k3)+k4)/6);
		x = x+h;
		cout << x << " " << y << " " << z << endl;
		
	}	

	return 0;
}

float ev_z(float x, float y){
	return -y;
}
float ev_y(float x, float y, float z){
	return z;
}