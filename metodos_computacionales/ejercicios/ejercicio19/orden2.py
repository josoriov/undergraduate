import numpy as np
import matplotlib.pyplot as plt


# In[2]:

A = np.loadtxt('orden2.txt')


x = A[:,0]
y = A[:,1]
z = A[:,2]

y_0 = np.cos(x)
err = abs(y_0 - y)

# In[4]:

# plt.scatter(x,y, color='gray')
plt.plot(x,err, color='blue')
plt.xlabel('X')
plt.ylabel('Error')
plt.savefig('segundo_orden.png')