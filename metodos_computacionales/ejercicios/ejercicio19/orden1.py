
import numpy as np
import matplotlib.pyplot as plt


# In[2]:

A = np.loadtxt('orden1.txt')


# In[3]:

x = A[:,0]
y = A[:,1]

y_0 = np.exp(-x)

err = abs(y_0-y)/y_0


# In[4]:

# plt.scatter(x,y, color='gray')
plt.plot(x,err, color='blue')
plt.xlabel('X')
plt.ylabel('Error porcentual')
plt.savefig('primer_orden.png')
