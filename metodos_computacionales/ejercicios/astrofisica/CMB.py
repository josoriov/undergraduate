
# coding: utf-8

# In[1]:

import numpy as np
import matplotlib.pyplot as plt


# In[2]:

A = np.loadtxt('raw_data.txt')


# In[3]:

freq0 = A[:,0]
S0 = A[:,1]

freq = A[:,0]*100
S = A[:,1]*(10**-20)


# In[12]:

plt.plot(freq0,S0, color='#39A0ED')
plt.xlabel(r'Frecuencia $(cm^{-1})$')
plt.ylabel(r'MJy/sr')
plt.savefig('espectro.png')


# In[5]:

# Definición de las constantes básicas en unidades del SI
c = 299792458 # Velocidad de la luz
h = 6.62607004 * (10**-34) # Constante de Planck
K_B = 1.38064852 * (10**-23) # Constante de Bolztmann


# In[6]:

arg = np.argmax(S)


# In[9]:

T = (freq[arg]*c*h)/(K_B*2.82)


# In[10]:

T


# In[ ]:



