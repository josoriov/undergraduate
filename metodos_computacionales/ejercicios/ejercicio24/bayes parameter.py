
# coding: utf-8

# In[15]:

import numpy as np
import matplotlib.pyplot as plt
from scipy import integrate


# In[23]:

def prob(x, l):
    return (np.exp(-x/l))/(l)


# In[44]:

prior = 0.8
x_list = [1.2,2.5,2.8,5.0]


# In[45]:

def bayes(l,x_list):
    res = 1
    for x in x_list:
        integral, error = scipy.integrate.quad(func = prob,a=1.0,b=20.0, args=(x))
        res = res*(integral*prior)
    return res


# In[46]:

l_list = np.linspace(0,100,500)

p_l = list()

for l in l_list:
    p_l.append(bayes(l,x_list))


# In[47]:

plt.plot(l_list,p_l)
plt.show()


# In[ ]:



