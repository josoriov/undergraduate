#include <iostream>
#include <cstdlib>
#include <ctime>

using std::cout;
using std::endl;

int main(){

  double c = 1.0, T = 0.5;
  double x = 0.0, t=0.0;
  double x_0 = 0.1, t_0 = 0.01;
  int N = (int)(T/t_0) + 1; // Puntos en t
  int M = (int)(2.0/x_0) + 1; // Puntos en x
  double ** datos = new double*[M];
  int i, j;
  // Inicializo el array que guarda los datos para cierto tiempo

  for(i=0;i<M;i++){
    datos[i] = new double[N];
  }

  for(i=0;i<N;i++){
    double x_i = i*x_0;

    if(x_i>=0.75 && x_i<=1.25){
      datos[0][i] = 1.0;
    }
    else{
      datos[0][i] = 0.0;
    }

  }
  

  for(j=0;j<M-1;j++){

    for(i=0;i<N;i++){


      if(i==0){

        datos[j+1][i] = datos[j][i];
      }
      else{        
        datos[j+1][i] = datos[j][i] - (c*t_0*(datos[j][i]-datos[j][i-1])/t_0);
      }
      cout << x_0*(j) << " " << datos[j][i] << endl;

    }
    
  }


  return 0;
}