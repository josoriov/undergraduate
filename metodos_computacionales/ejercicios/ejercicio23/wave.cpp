#include <iostream>
#include <cstdlib>
#include <ctime>
#include <math.h>

#define pi 3.14159265

using std::cout;
using std::endl;

int max_arg(double * datos, int M);

int main(){
  double c = 0.1, ini = 0.0;
  double t=0.0, x=0.0;
  double x_0 = 0.01, t_0 = 0.001;
  int M = (int)(1.0/x_0) + 1; // Puntos en x
  double * datos = new double[M];
  double * pre_datos = new double[M];
  int i, j;

  // Inicializo el array que guarda los datos para cierto tiempo
  for(i=0;i<M;i++){
    double x_i = x + (i*x_0);
    datos[i] = sin(2*pi*x_i);
    pre_datos[i] = 0.0;
  }

  int max = max_arg(datos,M);
  int n_t = 0;
  for(j=0;j<M;j++){
    cout << t << " " << ini + (x_0*j) << " " << datos[j] <<endl;
  }

  while(datos[max] >= -1){

    // Imprimiremos 4 snapshots con determinado tiempo para ver la evolución
    /*if(abs(datos[max]-1)<0.05 || abs(datos[max]-0.5)<0.05 || abs(datos[max]+0.5)<0.05 || abs(datos[max]+1)<0.05){
      for(j=0;j<M;j++){
        cout << t << " " << ini + (x_0*j) << " " << datos[j] <<endl;
      }
    }*/

    t = t_0*n_t;
    // For para evolucionar el array
    for(i=0;i<M;i++){
      // Evoluciono para el primer intervalo temporal
      if(n_t == 0){
        //Actualizo el array con los datos de la posición temporal inmediatamente anterior
        pre_datos[i] = datos[i];

        // Evoluciono el array de los datos en la posición temporal actual
        if(i==0 || i==(M-1)){
          datos[i] = datos[i];
        }
        else{
          // Evoluciona con central difference
          double temp = (t_0*t_0*c*c)/(2*x_0*x_0);
          double temp2 = temp*(datos[i+1] -(2*datos[i])-datos[i-1]);
          datos[i] = datos[i] - temp2;
        }

      }
      //Evoluciono para los demás intervalos temporales
      else{

        //Actualizo el array con los datos de la posición temporal inmediatamente anterior
        pre_datos[i] = datos[i];

        // Evoluciono el array de los datos en la posición temporal actual
        if(i==0 || i==(M-1)){
          datos[i] = datos[i];
        }
        else{
          // Evoluciona con central difference
          double temp = (t_0*t_0*c*c)/(2*x_0*x_0);
          double temp2 = temp*(datos[i+1] - (2*datos[i])+ datos[i-1]);
          datos[i] = (2*datos[i]) + pre_datos[i] - temp2;
        }

      }

    }

    n_t = n_t + 1;

  }

  return 0;
}

int max_arg(double * datos, int M){
  int i;
  int sol = 0;
  for(i=0;i<M;i++){
    (datos[sol] < datos[i]) ? (sol = i) : (sol = sol);
  }
  return sol;
}
