# coding: utf-8

# In[21]:

import numpy as np
import matplotlib.pyplot as plt


# In[22]:

A = np.loadtxt('data.txt')

x = A[:,1]
f = A[:,2]


# In[23]:

plt.plot(x,f)
plt.savefig('wave.png')


# In[ ]wave
