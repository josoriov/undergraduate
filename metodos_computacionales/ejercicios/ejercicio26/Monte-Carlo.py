
# coding: utf-8

# In[139]:

import numpy as np
import matplotlib.pyplot as plt


# In[140]:

v = 10*np.random.random(100000000) + 35


# In[141]:

fig = plt.figure()
ax = fig.add_subplot(111)
n, bins, rectangles = ax.hist(v, 50, normed=True)
ax.set_xlabel(r'$V$')
ax.set_ylabel(r'$P(V)$')
ax.set_title(u'Sin datos')
fig.canvas.draw()
plt.savefig('Original.png')


# In[142]:

theta = np.pi*0.5*np.random.random(100000000)


# In[143]:

fig = plt.figure()
ax = fig.add_subplot(111)
n, bins, rectangles = ax.hist(theta, 50, normed=True)
ax.set_xlabel(r'$\theta$')
ax.set_ylabel(r'$P(\theta)$')
ax.set_title(u'Sin datos')
fig.canvas.draw()
plt.savefig('original_theta.png')


# In[144]:

def distancia(v,theta):
    g = 9.8
    return ((v**2)*np.sin(2*theta))/g


# In[145]:

alcance = [61,115,31,177]


# In[146]:

for i in range(len(v)):
    d = distancia(v[i],theta[i])
    d_max = alcance[0] + 5.0
    d_min = alcance[0] - 5.0
    if(d > d_max or d < d_min):
        v[i] = 0


# In[147]:

v_1 = [vel for vel in v if vel != 0]
theta = np.pi*0.5*np.random.random(len(v_1))


# In[148]:

fig = plt.figure()
ax = fig.add_subplot(111)
n, bins, rectangles = ax.hist(v_1, 50, normed=True)
ax.set_xlabel(r'$V$')
ax.set_ylabel(r'$P(V)$')
ax.set_title(u'Un dato')
fig.canvas.draw()
plt.savefig('1dato.png')


# In[149]:

for i in range(len(v_1)):
    d = distancia(v_1[i],theta[i])
    d_max = alcance[1] + 5.0
    d_min = alcance[1] - 5.0
    if(d > d_max or d < d_min):
        v_1[i] = 0


# In[150]:

v_2 = [vel for vel in v_1 if vel != 0]
theta = np.pi*0.5*np.random.random(len(v_2))


# In[151]:

fig = plt.figure()
ax = fig.add_subplot(111)
n, bins, rectangles = ax.hist(v_2, 50, normed=True)
ax.set_xlabel(r'$V$')
ax.set_ylabel(r'$P(V)$')
ax.set_title(u'Dos datos')
fig.canvas.draw()
plt.savefig('2datos.png')


# In[152]:

for i in range(len(v_2)):
    d = distancia(v_2[i],theta[i])
    d_max = alcance[1] + 5.0
    d_min = alcance[1] - 5.0
    if(d > d_max or d < d_min):
        v_2[i] = 0


# In[153]:

v_3 = [vel for vel in v_2 if vel!=0]
theta = np.pi*0.5*np.random.random(len(v_3))


# In[154]:

fig = plt.figure()
ax = fig.add_subplot(111)
n, bins, rectangles = ax.hist(v_3, 50, normed=True)
ax.set_xlabel(r'$V$')
ax.set_ylabel(r'$P(V)$')
ax.set_title(u'Tres datos')
fig.canvas.draw()
plt.savefig('3datos.png')


# In[155]:

for i in range(len(v_3)):
    d = distancia(v_3[i],theta[i])
    d_max = alcance[1] + 5.0
    d_min = alcance[1] - 5.0
    if(d > d_max or d < d_min):
        v_3[i] = 0


# In[156]:

v_4 = [vel for vel in v_3 if vel!=0]
theta = np.pi*0.5*np.random.random(len(v_4))


# In[157]:

fig = plt.figure()
ax = fig.add_subplot(111)
n, bins, rectangles = ax.hist(v_4, 50, normed=True)
ax.set_xlabel(r'$V$')
ax.set_ylabel(r'$P(V)$')
ax.set_title(u'Cuatro datos')
fig.canvas.draw()
plt.savefig('4datos.png')


# In[158]:

for i in range(len(v_4)):
    d = distancia(v_4[i],theta[i])
    d_max = alcance[1] + 5.0
    d_min = alcance[1] - 5.0
    if(d > d_max or d < d_min):
        v_4[i] = 0


# In[159]:

v_5 = [vel for vel in v_4 if vel!=0]
theta = np.pi*0.5*np.random.random(len(v_5))


# In[160]:

fig = plt.figure()
ax = fig.add_subplot(111)
n, bins, rectangles = ax.hist(v_5, 50, normed=True)
ax.set_xlabel(r'$V$')
ax.set_ylabel(r'$P(V)$')
ax.set_title(u'Cinco datos')
fig.canvas.draw()
plt.savefig('5datos.png')


# In[ ]:



