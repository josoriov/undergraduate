#include <iostream>
#include <cstdlib>
#include <ctime>

using std::cout;
using std::endl;

using namespace std;

class Album{
public:
  void CompraSobre(int n_en_sobre);
  void Status(void);
  bool EstaLleno();
  int DarSobresComprados();
  int DarEnAlbum();
  int DarTotal();
  Album(int n);
  
private:
  int n_total;
  int n_repetidas;
  int n_en_album;
  int n_sobres_comprados;
  
  int *album;
  int *repetidas;
};

void Album::Status(void){
  cout << n_en_album << " " <<n_repetidas << endl;
}

void Album::CompraSobre(int n_en_sobre){
  int lamina;
  
  for(int i=0;i<n_en_sobre;i++){
    lamina = rand()%n_total;

    if(album[lamina]==0){
      album[lamina] = 1;
      n_en_album++;
    }
    else{
      repetidas[lamina] +=1;
      n_repetidas++;
    }
  }
  n_sobres_comprados = n_sobres_comprados + 1;
}

Album::Album(int n){
  album = new int[n];
  repetidas = new int[n];
  n_total = n;
  n_sobres_comprados = 0;
  n_repetidas = 0;
  n_en_album = 0 ;
  for (int i=0;i<n_total;i++){
    album[i] = 0;
    repetidas[i] = 0;
  }
}

bool Album::EstaLleno(){
	if(n_en_album == n_total){
		return true;
	}
	else{
		return false;
	}
}


int Album::DarSobresComprados(){
	return n_sobres_comprados;
}

int Album::DarEnAlbum(){
	return n_en_album;
}

int Album::DarTotal(){
	return n_total;
}

int main(){
  srand(time(0));
  Album A(660);
  int count = 0; 
  while(A.EstaLleno() == false){
  	A.CompraSobre(5);
  	count++;
  }
  cout << A.DarSobresComprados() << endl;
}
