#include <iostream>
#include <cstdlib>
#include <ctime>

using std::cout;
using std::endl;

using namespace std;

void cambio(Album A, Album B);

class Album{
public:
  void CompraSobre(int n_en_sobre);
  void Status(void);
  bool EstaLleno();
  int DarSobresComprados();
  int DarEnAlbum();
  int DarTotal();
  bool PuedoDar(int n);
  bool PuedoRecibir(int n);
  void QuitarRepetida(int n);
  void AgregarLamina(int n);
  Album(int n);
  
private:
  int n_total;
  int n_repetidas;
  int n_en_album;
  int n_sobres_comprados;
  
  int *album;
  int *repetidas;
};

Album::Album(int n){
  album = new int[n];
  repetidas = new int[n];
  n_total = n;
  n_sobres_comprados = 0;
  n_repetidas = 0;
  n_en_album = 0 ;
  for (int i=0;i<n_total;i++){
    album[i] = 0;
    repetidas[i] = 0;
  }
}

void Album::Status(void){
  cout << n_en_album << " " <<n_repetidas << endl;
}

void Album::CompraSobre(int n_en_sobre){
  int lamina;
  
  for(int i=0;i<n_en_sobre;i++){
    lamina = rand()%n_total;

    if(album[lamina]==0){
      album[lamina] = 1;
      n_en_album++;
    }
    else{
      repetidas[lamina] +=1;
      n_repetidas++;
    }
  }
  n_sobres_comprados = n_sobres_comprados + 1;
}

bool Album::EstaLleno(){
	if(n_en_album == n_total){
		return true;
	}
	else{
		return false;
	}
}


int Album::DarSobresComprados(){
	return n_sobres_comprados;
}

int Album::DarEnAlbum(){
	return n_en_album;
}

int Album::DarTotal(){
	return n_total;
}

bool PuedoDar(int n){
  if(repetidas[n] != 0){
    return true;
  }
  else{
    return false;
  }
}

bool PuedoRecibir(int n){
  if(album[n]==0){
    return true;
  }
  else{
    return false;
  }
}
void QuitarRepetida(int n){
  repetidas[n] = repetidas[n] - 1; 
}
void AgregarLamina(int n){
  album[n] = 1;
}




int main(){
  srand(time(0));
  Album A(660);
  Album B(660);
  int count = 0; 
  while(A.EstaLleno() == false){
  	A.CompraSobre(5);
    B.CompraSobre(5);
    cambio(A,B);
  	count++;
  }
  cout << A.DarSobresComprados() << endl;
}

void cambio(Album A, Album B){
  int N = A.DarTotal();
  int c_A = 0;
  int c_B = 0;

  for(int i=1 ; i<=N ; i++){
    if(A.PuedoDar(i) && B.PuedoRecibir(i)){
      c_A++;
    }
    else if(A.PuedoRecibir(i) && B.PuedoDar(i)){
      c_B++;
    }
  }
  int camb = ((c_A > c_B) ? c_A : c_B );
  int n = 0;

  for(int i=1 ; i<=N ; i++){
    int cambio_A = 0;
    int cambio_B = 0;

    while(cambio_A < camb){
      if(A.PuedoDar(i) && B.PuedoRecibir(i)){
        A.QuitarRepetida(i);
        B.AgregarLamina(i);
        cambio_A++;
      }
    }

    while(cambio_B < camb){
      if(A.PuedoRecibir(i) && B.PuedoDar(i)){
        A.AgregarLamina(i);
        B.QuitarRepetida(i);
        cambio_B++;
      }
    }

  }

}
