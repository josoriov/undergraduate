
# coding: utf-8

# In[12]:

import numpy as np
import matplotlib.pyplot as plt


# In[13]:

A = np.loadtxt('caos.txt')


# In[14]:

x = A[:,0]
y = A[:,1]


# In[18]:

plt.plot(x,y)
plt.savefig('caos.pdf')


# In[ ]:



