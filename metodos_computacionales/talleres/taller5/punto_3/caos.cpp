#include <iostream>
#include <cstdlib>
#include <ctime>
#include <math.h>

using std::cout;
using std::endl;

float ev_q1(float p1);
float ev_q2(float p2);
float ev_p1(float q1, float epsilon);
float ev_p2(float q1, float q2, float epsilon);


int main(){
	float t = 0.0, h = 0.006;
	float a = 1/(2 * (sqrt(2)) );
	float q1 = a, p1 = 0.0;
	float q2 = -a, p2 = 0.0;
	float epsilon = 0.001;

	float k1_p1, k2_p1, k3_p1, k4_p1;
	float k1_p2, k2_p2, k3_p2, k4_p2;
	float k1_q1, k2_q1, k3_q1, k4_q1;
	float k1_q2, k2_q2, k3_q2, k4_q2;

	while(t<=3000){

		bool positivo = true;

		k1_q1 = ev_q1(p1);
		k1_q2 = ev_q2(p2);
		k1_p1 = ev_p1(q1, epsilon);
		k1_p2 = ev_p2(q1, q2, epsilon);

		k2_q1 = ev_q1(p1+(k1_p1/2));
		k2_q2 = ev_q2(p2+(k1_p2/2));
		k2_p1 = ev_p1(q1+(k1_q1/2), epsilon);
		k2_p2 = ev_p2(q1+(k1_q1/2), q2+(k1_q2/2), epsilon);

		k3_q1 = ev_q1(p1+(k2_p1/2));
		k3_q2 = ev_q2(p2+(k2_p2/2));
		k3_p1 = ev_p1(q1+(k2_q1/2), epsilon);
		k3_p2 = ev_p2(q1+(k2_q1/2), q2+(k2_q2/2), epsilon);

		k4_q1 = ev_q1(p1+k3_p1);
		k4_q2 = ev_q2(p2+k3_p2);
		k4_p1 = ev_p1(q1+k3_q1, epsilon);
		k4_p2 = ev_p2(q1+k3_q1, q2+k3_q2, epsilon);

		q1 = q1 + (h*(k1_q1+(2*k2_q1)+(2*k3_q1)+k4_q1)/6);
		q2 = q2 + (h*(k1_q2+(2*k2_q2)+(2*k3_q2)+k4_q2)/6);
		p1 = p1 + (h*(k1_p1+(2*k2_p1)+(2*k3_p1)+k4_p1)/6);
		p2 = p2 + (h*(k1_p2+(2*k2_p2)+(2*k3_p2)+k4_p2)/6);		

		t = t+h;

		(q1 > 0) ? (positivo = true) : (positivo = false);

		if(positivo == true){
			cout << p2 << " " << q2 << endl;
		}
		
	}
		

	return 0;
}

float ev_q1(float p1){
	return p1;
}

float ev_q2(float p2){
	return p2;
}

float ev_p1(float q1, float epsilon){
	float temp = (4*q1*q1) + (epsilon*epsilon);
	float temp2 = sqrt((temp*temp*temp));

	return - (2*q1)/temp2;
}

float ev_p2(float q1, float q2, float epsilon){
	float var1 = q1 - q2;
	float var2 = q1 + q2;
	float var3 = epsilon/2;

	float den1 = (var1*var1) + (var3*var3);
	float den2 = (var2*var2) + (var3*var3);

	float temp1 = sqrt(pow(den1, 3.0));
	float temp2 = sqrt(pow(den2, 3.0));

	return (var1/temp1) - (var2/temp2);
}