#include <iostream>
#include <cstdlib>
#include <ctime>
#include <math.h>

#define pi 3.14159265

using std::cout;
using std::endl;

int max_arg(double * datos, int M);
double ** generar(double x, double L, double x_0, int M, int N);

int main(){
  double L = 100.0, ro = 10, T = 40;
  double c = sqrt(T/ro);
  double t=1.0, x=0.0;
  double x_0 = 1.0, t_0 = 0.05;
  int M = (int)(100.0/x_0) + 1; // Puntos en x
  int N = (int)(198.0/t_0) + 1; // Puntos en y
  double temp = (t_0*t_0*c*c)/(x_0*x_0);
  double ** datos = generar(x, L, x_0, M, N);

  int i, j;


  int n_t = 1;

  // Imprimo la posición inicial de la cuerda
  for(i=0;i<M;i++){
    cout << t << " " << x_0*i << " " << datos[0][i] <<endl;
  }

  for(j=0;j<(N-1);j++){    

    // Ciclo para evolucionar el array
    for(i=0;i<M;i++){
      

      // Evoluciono para el primer intervalo temporal
      if(j == 0){

        // Evoluciono el array de los datos en la posición temporal actual
        if(i==0 || i==(M-1)){
          datos[j+1][i] = datos[j][i];
        }
        else{
          // Evoluciona con forward difference
          double temp2 = temp*(datos[j][i+1] -(2*datos[j][i]) + datos[j][i-1])/2;
          datos[j+1][i] = datos[j][i] + temp2;
        }
        

      }

      //Evoluciono para los demás intervalos temporales
      else{

        // Evoluciono el array de los datos en la posición temporal actual
        if(i==0 || i==(M-1)){
          datos[j+1][i] = datos[j][i];
        }
        else{
          // Evoluciona con forward difference
          double temp2 = temp*(datos[j][i+1] - (2*datos[j][i]) + datos[j][i-1]);
          datos[j+1][i] = (2*datos[j][i]) - datos[j-1][i] + temp2;
        }

      }

    }

    if(j%5 == 0){
      for(i=0;i<M;i++){
        cout << t+ (j*t_0) << " " << x_0*i << " " << datos[j][i] <<endl;
      }      
    }


    n_t = n_t + 1;
    

  }

  return 0;
}


// Función que genera los datos iniciales de la cuerda
double ** generar(double x, double L, double x_0, int M, int N){

  double ** datos = new double*[N];
  int i,j;

  for(j=0;j<N;j++){
    datos[j] = new double [M];
  }

  // Inicializo el array que guarda los datos para cierto tiempo
  for(j=0;j<N;j++){

    for(i=0;i<M;i++){
      double x_i = x + (i*x_0);

      if(j==0){
        if(x_i <=(0.8*L)){
          datos[j][i] = 1.25*x_i/L;
        }
        else{
          datos[j][i] = 5 - (5*x_i/L);
        }
      }
      else{
        datos[j][i] = 0;
      }

    }

  }


  return datos;

}