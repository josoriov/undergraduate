
# coding: utf-8

# In[1]:

import numpy as np
import matplotlib.pyplot as plt


# In[2]:

A = np.loadtxt('cuerda.txt')


# In[3]:

t_val = list()


# In[4]:

for i in range(len(A[:,0])):    
    if(A[i,0] not in t_val):
        t_val.append(A[i,0])


# In[5]:

def split_array(array, cond):
    return array[cond]


# In[6]:

filenames = list()


# In[7]:

for i in range(1,len(t_val)):
    temp = np.zeros((101,2))
    temp = split_array(A, A[:,0] == t_val[i])
    temp = np.delete(temp,0,1)
    
    
    x = temp[:,0]
    y = temp[:,1]
    
    name = 'fig' + str(i) + '.png'
    filenames.append(name)
    
    plt.plot(x,y)
    plt.xlim(0,100)
    plt.ylim(-2,2)
    plt.savefig(name)
    plt.close()


# In[8]:

# Esta parte de código fue tomada para corregir un bug que había con la librería imageio.
# De esta forma, no se presentan runtime errors cuando intenta leer las imágenes (imread)
from PIL import Image
def register_extension(id, extension): Image.EXTENSION[extension.lower()] = id.upper()
Image.register_extension = register_extension
def register_extensions(id, extensions): 
  for extension in extensions: register_extension(id, extension)
Image.register_extensions = register_extensions


# In[9]:

import imageio


# In[10]:

images = []
for filename in filenames:
    images.append(imageio.imread(filename))
imageio.mimsave('cuerda.gif', images)





