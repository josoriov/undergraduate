#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cmath> 
#include <math.h>

#define pi 3.14159265

using std::cout;
using std::endl;

double ** generar_datos(int M, double V, double l, double h);

int main(){

	double V = 100.0; // Valor de potencial inicial
	double L = 5.0; // Longitud del área a discretizar
	double l = 2.0; // Longitud de las placas
	double d = 1.0; // Distancia entre placas
	double h = 5.0/512.0; // Valor de discretización	
	int N = 2*512*512; // Número de iteraciones
	int M = (int)(L/h) + 1; // Número de puntos por dimensión
	int i,j; // Índices para hacer recorridos sobre el array

	double ** datos =  generar_datos(M,V,l,h);
	double ** datos_temp = generar_datos(M,V,l,h);

	

	while(N>0){
		for(i=1;i<(M-1);i++){
			for(j=1;j<(M-1);j++){
				double temp = datos[i+1][j] + datos[i-1][j] + datos[i][j+1] + datos[i][j-1];
				datos_temp[i][j] = temp/4.0;
			}
		}

		datos = datos_temp;
		/*
		for(i=1;i<(M-1);i++){
			for(j=1;j<(M-1);j++){
				datos[i][j] = datos_temp[i][j];
			}
		}*/
		N = N - 1;
	}

	for(i=0;i<M;i++){
		for(j=0;j<M;j++){
			cout << datos[i][j] << " ";
		}
		cout << endl;
	}

	
	return 0;
}

double ** generar_datos(int M, double V, double l, double h){
	int i,j;

	double ** datos = new double*[M];

	for(i=0;i<M;i++){
		datos[i] = new double [M];
	}

	// Recorridos para llenar la matriz con las condiciones iniciales
	for(i=0;i<M;i++){
		for(j=0;j<M;j++){
			// Guardamos la posición espacial donde debería estar la posición [i,j]
			double pos_x = -2.5 + (i*h);
			double pos_y = -2.5 + (j*h);

			// Construimos la placa que va arriba del 0 en el eje x
			if(abs(pos_x)<=(l/2) && abs(pos_y - 0.5) < (h) ){
				datos[i][j] = (V/2);
			}
			// Construimos la placa que va abajo del 0 en el eje x
			else if(abs(pos_x)<=(l/2) && abs(pos_y + 0.5) < (h) ){
				datos[i][j] = -(V/2);
			}
			// Si no es ninguna de las dos placas, el valor del potencial es cero
			else{
				datos[i][j] = 0.0;
			}
		}
	}

	return datos;
}