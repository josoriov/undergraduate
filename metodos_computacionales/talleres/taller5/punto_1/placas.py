
# coding: utf-8

# In[1]:

import numpy as np
import matplotlib.pyplot as plt


# In[2]:

A = np.loadtxt('placas.txt')


# In[3]:

plt.imshow(A,origin='lower')
plt.suptitle(u'Potencial eléctrico')
plt.savefig('potencial.png')
plt.close()


# In[4]:

M,N = np.shape(A)

E_x = np.zeros((M,N))
E_y = np.zeros((M,N))


# In[5]:

h = 5.0/512.0

for i in range(1,M-1):
    for j in range(1,M-1):
        E_x[i,j] = -(A[i+1,j]-A[i-1,j])/(2*h)
        E_y[i,j] = -(A[i,j+1]-A[i,j-1])/(2*h)


# In[6]:

E = np.sqrt((E_x**2) + (E_y**2))


# In[7]:

plt.imshow(E)
plt.suptitle(u'Campo eléctrico')
plt.savefig('lineas_campo.png')
plt.close()


# In[8]:

Y, X = np.mgrid[-2.5:2.5:513j, -2.5:2.5:513j]
plt.streamplot(X,Y, E_y.T, E_x.T, density=2, color='#9DB5B2')
plt.suptitle(u'Lineas de campo')
plt.savefig('campo.png')
plt.close()


# In[ ]:



