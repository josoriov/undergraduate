
# coding: utf-8

# In[105]:

import matplotlib.pyplot as plt
import numpy as np
from scipy import ndimage
from PIL import Image
import sys


# In[106]:
filename = str(sys.argv[1])

im = Image.open(filename).convert('LA')
data = np.array(im, dtype=float)
x,y,z = np.shape(data)


# In[107]:

# Kernel sencillo de un filtro pasa altas
kernel_pa = np.array([[-1, -1, -1, -1, -1],
                   	  [-1,  1,  2,  1, -1],
                      [-1,  2,  4,  2, -1],
                      [-1,  1,  2,  1, -1],
                      [-1, -1, -1, -1, -1]])


# In[108]:

#Kernel sencillo de un filtro pasa bajas
kernel_pb = np.array([[1, 1, 1, 1, 1],
                      [1, 1, 1, 1, 1],
                      [1, 1, 1, 1, 1],
                      [1, 1, 1, 1, 1],
                      [1, 1, 1, 1, 1]])


# In[109]:
tipo = str(sys.argv[2])

dat = data[:,:,0]
matriz = np.zeros((x,y))
nombre = ''
if(tipo == 'alto'):
	matriz = ndimage.convolve(dat, kernel_pa)	
	nombre = 'altas.png'

elif(tipo == 'bajo'):
	matriz = ndimage.convolve(dat, kernel_pb)
	nombre = 'bajas.png'




# In[110]:
plt.imshow(matriz)
plt.axis('off')
plt.savefig(nombre, bbox_inches='tight')


# In[ ]:



