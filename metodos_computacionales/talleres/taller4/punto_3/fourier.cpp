#include <iostream>
#include <cstdlib>
#include <ctime>
#include <sstream>
#include <string>
#include <fstream>
#include <math.h>
#include <stdio.h>

# define pi 3.14159265

using namespace std;

int contar_lineas(const char* filename);
double ** obtener_datos(const char* filename);
double * obtener_x(double ** datos, const char* filename);
double * obtener_y(double ** datos, const char* filename);
double ** fourier(double * y, const char* filename);
double maximo_valor(double * x, const char* filename);
double minimo_valor(double * x, const char* filename);
double ** lagrange(double ** datos, const char* filename);


int main(int argc, char *argv[]){
	const char* archivo = argv[1];

	double ** datos = obtener_datos(archivo);
	double ** datos_interpol = lagrange(datos,archivo);
	double * y = obtener_y(datos_interpol, archivo);
	double ** transform = fourier(y, archivo);
	int n_lineas = contar_lineas(archivo);

	int i;

	ofstream myfile ("transformada.txt");
	for(i=0;i<n_lineas;i++){
		myfile << transform[0][i] << " " << transform[1][i] << " " << transform[2][i] << endl;
	}
	
	myfile.close();
	return 0;
}

// Función que cuenta el número de líneas
int contar_lineas(const char * filename){
	int n_lineas = 0;
    string line;
    ifstream myfile(filename);

    while (getline(myfile, line))
    	n_lineas = n_lineas + 1;
    return n_lineas;
}
// Función que obtiene los datos del archivo con el nombre dado
double ** obtener_datos(const char* filename){
	int n_lineas = contar_lineas(filename);	
	double ** datos = new double*[2];

    // Inicializamos el array que contendrá los datos
    int i;	
	for(i = 0;i<2;i++){
		datos[i] = new double[n_lineas];
	}
	
	// Procesamos línea por línea el txt. Llamamos a y b a los elementos de la primera y segunda columna, respectivamente.
	int j = 0;
	ifstream myFile(filename);
	string line;
	while (getline(myFile, line))
	{
	    istringstream iss(line);
	    double a, b;
	    if (!(iss >> a >> b)) {
	    break; 
	    }
	    // Procesamos el par (a,b) 
	    datos[0][j] = a;
	    datos[1][j] = b;
	    j++;
	    
	}
	return datos;
}

double * obtener_x(double ** datos, const char* filename){
	int n_lineas = contar_lineas(filename);

	// Inicializamos el array que contendrá los datos x
	double * x = new double[n_lineas];
	int i;
	for(i=0;i<n_lineas;i++){
		// Llenamos el array con los datos correspondientes
		x[i] = datos[0][i];
	}
	return x;
}

double * obtener_y(double ** datos, const char* filename){
	int n_lineas = contar_lineas(filename);

	// Inicializamos el array que contendrá los datos y
	double * y = new double[n_lineas];
	int i;
	// Llenamos el array con los datos correspondientes
	for(i=0;i<n_lineas;i++){
		y[i] = datos[1][i];
	}
	return y;
}

// Función que obtiene la transformada de fourier de los datos pasados como parámetro.
double ** fourier(double * y, const char* filename){
	double dt = 0.1;
	int N = contar_lineas(filename);
	double ** transform = new double*[3];
	int k;
	int n;

	// Inicializamos el array que contendrá los datos
    int i;	
	for(i = 0;i<3;i++){
		transform[i] = new double[N];
	}

	for(k=0;k<N;k++){
		double real = 0;
		double imaginario = 0;
		for(n=0;n<(N-1);n++){
			double dft_re = cos (2*pi*k*n/N);
			double dft_im = sin (2*pi*k*n/N);

			real = real + (y[n]*dft_re);
			imaginario = imaginario + (y[n]*dft_im);
		}
		double freq = ((k+1)*dt)/((double)N);
		transform[0][k] = freq;
		transform[1][k] = real;
		transform[2][k] = imaginario;
	}

	// Retorna el array que contiene las frecuencias, la parte real y la parte imaginaria.
	return transform;
}

double maximo_valor(double * x, const char* filename){
	int n_lineas = contar_lineas(filename);
	double maximo_valor;
	int i;
	for(i=0;i<n_lineas;i++){
		if(i==0){
			maximo_valor = x[i];
		}
		else{
			(maximo_valor < x[i]) ? (maximo_valor = x[i]) : (maximo_valor = maximo_valor);
		}
	}

	return maximo_valor;
}

double minimo_valor(double * x, const char* filename){
	int n_lineas = contar_lineas(filename);
	double minimo_valor;
	int i;
	for(i=0;i<n_lineas;i++){
		if(i==0){
			minimo_valor = x[i];
		}
		else{
			(minimo_valor > x[i]) ? (minimo_valor = x[i]) : (minimo_valor = minimo_valor);
		}
	}

	return minimo_valor;
}

double ** lagrange(double ** datos, const char* filename){
	int n_lineas = contar_lineas(filename);
	double * x = new double[n_lineas];
	double * y = new double[n_lineas];

	double * x_interpol = new double[n_lineas];
	double * y_interpol = new double[n_lineas];

	double ** interpol = new double*[2];

	int i,j,k;
	// Lleno los arrays de datos con los valores correspondientes 
	for(i=0;i<n_lineas;i++){
		x[i] = datos[0][i];
		y[i] = datos[1][i];
	}

	for(i=0;i<2;i++){
		interpol[i] = new double[n_lineas];
	}

	double max = maximo_valor(x,filename);
	double min = minimo_valor(x,filename);
	double interval = (max-min)/(double)n_lineas;
	// Creo el intervalo equiespaciado
	for(i=0;i<n_lineas;i++){
		x_interpol[i] = min + ((double)i*interval);
	}
	// Obtengo los puntos en y usando el intervalo equiespaciado
	for(k=0;k<n_lineas;k++){
		double term = 0.0;

		for(i=0;i<n_lineas;i++){		
			double prod = y[i];

			for(j=0;j<n_lineas;j++){
				
				if(i!=j){
					prod = prod*(x_interpol[k]-x[j])/(x[i]-x[j]);
				}				
			}
			term = term + (prod);
		}

		y_interpol[k] = term;
	}

	for(i=0;i<n_lineas;i++){
		interpol[0][i] = x_interpol[i];
		interpol[1][i] = y_interpol[i];
	}

	return interpol;
}