
# coding: utf-8

# In[140]:

import numpy as np
import matplotlib.pyplot as plt
from PIL import Image
import sys


# In[141]:

img = plt.imread('meme.png')


# In[142]:

# Calcula un kernel gaussiano 1D basado en un ancho dado como parámetro
def ancho_de_filtro(N):
    puntos = [(float(3 * i)/float(N))**2.0 for i in range(-N,N + 1)]
    fact = [np.exp(-(i/2.0)) for i in puntos]
    ks = float(sum(fact))
    kernel = [j/ks for j in fact]
    return kernel

# Función filtra con el kernel dado para un solo punto
def filtrar(ind,data,ancho):
    N = (len(ancho)-1)//2
    
    array = [data[0] for i in range(N)] + data + [data[-1] for i in range(N)]
    result = sum((float(array[N + ind + i]) * ancho[N+i] for i in range(-N,N+1)))
    return result

# Función filtra con el kernel dado para un array de puntos
def filtro_gaussiano(data,ancho,filtro = filtrar):
    ret = []
    for i in range(len(data)):
        ret.append(filtro(i,data,ancho))
    return ret


# In[143]:

def sumar_pixeles(pixel1,pixel2):
    tup = tuple([pixel1[i]+pixel2[i] for i in range(len(pixel1))])
    return tup


# Aplicamos el filtrado componente a componente en el pixel
from functools import reduce
def filtrar_pixel(ind,data,ancho):
    N = (len(ancho)-1)//2
    
    array = [data[0] for i in range(N)] + data + [data[-1] for i in range(N)]
    
    filt = reduce(sumar_pixeles,(tuple([float(v) * ancho[N+i] for v in array[N + ind + i]]) for i in range(-N,N+1)))
    return filt


# In[144]:

# Aplica el filtro gaussiano en 2D haciendo un barrido por líneas
def filtro2D(data,ancho, filtro = filtrar):
    result = []
    
    for i in range(len(data)):
        result.append(filtro_gaussiano(data[i],ancho,filtro))
        
    #Aplicamos un flitro 1D línea por líneas
    for i in range(len(data[0])):
        temp = filtro_gaussiano([result[t][i] for t in range(len(data))], ancho,filtro)

        for t in range(len(data)):
            result[t][i] = temp[t]

    return result


# In[145]:
# Aplicamos el filtro gaussiano 2D al archivo con el nombre dado y generando el kernel
def suavizado(filename,n_pixel):
    img = Image.open(filename)
    M,N = img.size

    ancho = ancho_de_filtro(n_pixel)

    #Creamos un array donde se almacena la información RGB de los pixeles.
    pixeles = filtro2D([[img.getpixel((w,h)) for w in range(M)] for h in range(N)],ancho,filtrar_pixel)
    
    # Creamos la imagen en blanco con las dimensiones originales
    imagen = Image.new("RGB",(M,N))
    
    #LLenamos uno a uno los pixeles de la imagen con los valores del array creado anteriormente
    for h in range(N):
        for w in range(M):
            imagen.putpixel((w,h),(int(pixeles[h][w][0]),int(pixeles[h][w][1]),int(pixeles[h][w][2])))
    imagen.save('suave.png')


# In[148]:

entrada = str(sys.argv[1])
n_pixel = int(sys.argv[2])
suavizado(entrada,n_pixel)





